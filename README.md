# Diablo 3 Keep Alive

## Installation

You have 2 options:

1. Download [AutoIt](http://www.autoitscript.com/site/autoit/) and [d3chat.au3](https://bitbucket.org/ggutenberg/diablo-3-keep-alive/src/master/d3chat.au3) (or fork this repo).
2. Download [d3chat.exe](https://bitbucket.org/ggutenberg/diablo-3-keep-alive/downloads).

If you choose the second option, your antivirus program might complain about it being a virus.  It's not.  Scripts compiled with AutoIt tend to get false positives from virus vendors.  If you don't trust the .exe, go with option 1.

## Usage
If you went with option 1 and installed AutoIt, you should have a file association for .au3 files.  Double-click d3chat.au3.

If you went with option 2, double-click d3chat.exe.

You will be asked how long you would like the delay between pings.  Select something that's not too small (don't want to slam Blizzard's servers).  Somewhere between 60 - 300 seconds should be good (1 to 5 minutes).

You will then be asked what the message you'd like to send to the chat is.  I defaulted to "ping" because that's essentially what you're doing, but you can put anything in here.  Try to keep it short and avoid profanities.  This is Global Chat after all, and there might be youngins playing.

Start Diablo 3.  Log in, pick your character, start a game.  Change the chat to a Global channel.  General or whatever.

Hit F9 to begin sending chat messages.  A message will be sent every **n** seconds where **n** is the number you entered above.

To stop sending messages, hit F9 again.

To start sending them again, hit F9.

To exit the script hit F10.

## General

I wrote this script for a friend of mine who was having a lot of trouble.  He said it worked for him.  I haven't had these problems, so can't really troubleshoot if something isn't working.  But if you have suggestions, comments, etc., hit me up on [my blog](http://grinninggecko.com/diablo-3-keep-alive-error-3007/).